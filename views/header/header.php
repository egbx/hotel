<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "page-container" div.
 */
?><!DOCTYPE html>
<html {{language}} class="no-js">
<head>
	<meta charset="{{charset}}">
	<meta name="viewport" content="width=device-width">
	<meta name="viewport" content="initial-scale=1">	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!--<link rel="pingback" href="{{pingback}}">-->

	<link rel="icon" href="/assets/img/favicon.ico?" type="image/x-icon" />
	<link rel="shortcut icon" href="/assets/img/favicon.ico" />
	<link href="/assets/css/style.css" rel="stylesheet" />
</head>
<body>
<div id="page-container" >