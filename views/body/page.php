<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 *
 */
\App\Layout\Class_Layout::getTemplatePart('header'); ?>
<div id="main-content" class="site-content" role="article" >
    <?php ///\App\Layout\Get::sidebar('content', $layout); ?>
    <main id="main" class="site-main" >
    <?php 
        // Include the page content template.
        \App\Layout\Class_Layout::getTemplatePart('content');
    ?>
    </main><!-- .site-main -->
</div>    
<?php #\App\Layout\Class_Layout::getTemplatePart('banner'); ?>
<?php \App\Layout\Class_Layout::getTemplatePart('footer'); ?>