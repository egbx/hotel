<?php
namespace App\Content;

use \App\Config as Config;
use \App\Fields as Fields;

class Class_Content extends \App\Init\Class_Init {

	public $selected_content = array();

	protected static $content = array(

		'index' => array(
			'page_title' => 'Hotel Form',
			'page_content' => '<h2>Testing the Hotel Form Here</h2>',

		),

		'test' => array(

			'page_title' => 'Test only',
			'page_content' => '<h2>This is a Another Page</h2>'

		),

		'results' => array(

			'page_title' => 'Our results from the search are...',
			'page_content' => '<h2>Return String:</h2>'

		),

	);

	public function __construct(){

		$requested_url = parent::$requested_url;
		
		if($requested_url) {
			
			$requested_url = str_replace('/','',$requested_url);

			return $this->buildContent($requested_url);
		}
	}

	public function buildContent($requested_url = null){

		$this->selected_content['requested_url'] = $requested_url;
		
		$get_template = ("" !== $requested_url) ? $requested_url : 'index';

		$this->selected_content['content'] = self::$content[$requested_url];

		/**
		 * Get the form and / or results based on the url.. note our restults methos does the curl call
		 */
		switch($this->selected_content['requested_url']) {
			case 'index' :
				$this->selected_content['content']['form'] = self::buildForm();
				break;
			case 'results' :
				$this->selected_content['content']['results'] = self::buildResults();
				break;
		}


		return $this->selected_content;

	}


	/**
	 * Builds the form and gets the results
	 * TODO: put these in their own Classes (rename Fields to Forms)
	 */
	public static function buildForm(){

		$form  = NULL;
		$form  = '<form method="POST" name="test_hotels" action="/results" >';
		/**
		 *	do loop for exiting items...
		 */
		$settingsFields = array_merge(Config\Config::$settingsFields,Config\Config::$customSettingsFields);
		$i = 0;

		foreach($settingsFields as $field => $field_options){

			/// Default to text field... if not set in config
			
			$field = Fields\Class_Fields::getFieldDefault($field, $field_options);
			$field_name = key($field);

			///$field_value = ${$field_name} = ( get_option($field_name) !== false) ? get_option($field_name) : NULL;
			$field_value = ${$field_name} = $field_options['value'];
			$field_options = $field[key($field)];

			$form .= '<div class="wrap" >';
			$form .= '<div class="wrap row" >';
							
			$form .= Fields\Class_Fields::returnField($i, $field_name, $field_value, $field_options);

			$form .= '</div>';
			$form .= '</div>';

			$i++;
		}

		$form .= '<div class="wrap" >';
		$form .= '<div class="wrap row" >';
						
		$form .= '<input type="submit" value="Search" />';

		$form .= '</div>';
		$form .= '</div>';
		$form .= '</form>';

		return $form ;
	}

	private static function buildResults(){

		///$content_to_return = '<ul class="hotel-results" >';
		$content_to_return = array();

		// Build data and send to curl...
		$data_to_send = array('post' => parent::$requested_url);

		///https://apis.ihg.com/guest-api/v1/ihg/us/en/searchLight
		$data_to_send['url'] = Config\Config::$settings['init_search']['base_url'] . Config\Config::$settings['init_search']['lang'] . Config\Config::$settings['init_search']['endpoint'];

		foreach($_POST as $k => $v){
			$data_to_send['data'][$k] = $v;

		}

		/// Get some data for later IE how many nights between dates..
		/**
		 * Send for a post for our initial search
		 * @var [type]
		 */
		# CURL 1 - GET HOTELS
		$results = new \App\Curl\Class_Curl($data_to_send);

		$current_results = json_decode($results->response);

		//// Now we submit the hotel code and get our next query for each hotel...
		if($current_results->result === 'success'){

			$data = json_decode($current_results->data);
			
			foreach($data->hotels as $hotelK => $hotel){

				$rate = ($hotel->rateRange->high > 0) ? $hotel->rateRange->high : $hotel->rateRange->low;

				$arr = array(
					'CurrencyCode' => $hotel->lowestPointsCurrencyCode,
					'NightlyRate' => number_format($rate, 2, '.', ','),
				);


				$data_to_send['url'] = Config\Config::$settings['details_search']['base_url'] . $hotel->hotelCode . '/' . Config\Config::$settings['details_search']['endpoint'];

				# CURL 2 : Get details on hotel based on its code...
				$results = new \App\Curl\Class_Curl($data_to_send, 'GET');
				$the_hotel = json_decode($results->response);
				$the_hotel_data = json_decode($the_hotel->data);

				###var_dump($the_hotel_data->hotelInfo->tax->total->items[0]->amount);

				$tax 							= $the_hotel_data->hotelInfo->tax->total->items[0]->amount->fee;
				$tax_basis						= $the_hotel_data->hotelInfo->tax->total->items[0]->amount->basis;
				$tax_included_in_rate			= $the_hotel_data->hotelInfo->tax->total->items[0]->amount->includedInRate;

				$individual_fee					= $the_hotel_data->hotelInfo->tax->individual->items[0]->amount->fee;
				$individual_basis				= $the_hotel_data->hotelInfo->tax->individual->items[0]->amount->basis;
				$individual_included_in_rate	= $the_hotel_data->hotelInfo->tax->individual->items[0]->amount->includedInRate;

				$service_charge 				= $the_hotel_data->hotelInfo->tax->serviceCharge->percentage;
				
				/**
				 * work out when P that is a percentage indicator
				 * we could cascade calculations here
				 */
				switch(true){

					case ($tax_included_in_rate) :
						/// No Calculation
						$tax = 1;
						break;

					case ($tax_basis === 'P') :
						$tax = intval('1.'.$tax);
						break;
					default :
						$tax = floatval($tax);
						break;
				}

				switch(true){

					case ($individual_included_in_rate) :
						/// No Calculation
						$individual_fee = 1;
						break;

					case ($individual_basis === 'P') :
						$individual_fee = intval('1.'.$individual_fee);
						break;
					default :
						$individual_fee = floatval($individual_fee);
						break;
				}

				$nights 					= self::calcDays($data_to_send['data']['start'], $data_to_send['data']['end']);
				$arr['nights'] 				= $nights;



				$total_costs_before_tax	 	= number_format( ( $rate * $individual_fee * $nights ) , 2, '.', ',');
				$total_costs_after_tax 		= number_format( ( $total_costs_before_tax * $tax ) , 2, '.', ',');

				$policies			= 'check In Time: ' . $the_hotel_data->hotelInfo->policies->checkinTime . '<br />';
				$policies			.= 'check Out Time: ' . $the_hotel_data->hotelInfo->policies->checkoutTime. '<br />';
				$policies			.= 'Service Charge Details: ' . $the_hotel_data->hotelInfo->policies->serviceChargeDetail->description . '<br />';
				
				$arr['RoomName'] = $the_hotel_data->hotelInfo->brandInfo->brandName;
				$arr['TotalBeforeTax'] = $total_costs_before_tax;
				$arr['TotalAfterTax'] = $total_costs_after_tax;
				$arr['CancellationTerms'] = $policies;

				$content_to_return['Rooms'][] = $arr;

			}
			
			
		}

		return '<textarea cols="50" rows="20" >' . json_encode($content_to_return) . '</textarea>';

	}

	/**
	 * Gets Date Calculation...
	 * TODO: put in own class
	 */
	//public static function ($order_id, $order_date, $return_date_format){
	public static function calcDays($booking_date_from, $booking_date_to, $return_date_format = '%d'){

		//// Fix error on order date error where does not accept / as contradicts Americans.
		$booking_date_from 	= str_replace('/', '-', $booking_date_from);
		$booking_date_to 	= str_replace('/', '-', $booking_date_to);

		$booking_date_from	=	date_create($booking_date_from);
		$booking_date_to	=	date_create($booking_date_to);
		$booking_date_diff	=	date_diff($booking_date_from,$booking_date_to);					
		
		return $booking_date_diff->format($return_date_format);

	} /// end collectionDate Method

}
