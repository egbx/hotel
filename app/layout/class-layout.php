<?php

namespace App\Layout;

use \App\Config as Config;
use \App\Content as Content;

class Class_Layout extends \App\Init\Class_Init {

	///public $current_view = array();

	protected static $layouts = array(

		'header' => array('template' => 'header',),
		'footer' => array('template' => 'footer',),
		'pages' => array(
			'index' => array(
				'template' => 'page-form',
				
			),

			'test' => array(
				'template' => 'page',
			),

			'results' => array(
				'template' => 'page-results',
			),
		)

	);

	public function __construct(){

		//return $this->buildLayout();
		$this->buildLayout();
		$this->createView();


	}

	protected function buildLayout() {

		
		parent::$current_view = self::$layouts;

		parent::$current_view['view'] = self::$layouts['pages'][parent::$requested_url];
		
		#return $current_view;

	}

	public function createView(){

		if("" !== (parent::$current_view['view']['template'])) {
			require_once($_SERVER['DOCUMENT_ROOT'] . '/views/body/' . parent::$current_view['view']['template'] . '.php');
		}

	}

	/**
	 * Is a call from the views to get the templates or partial templates, defaults back to the folder name.
	 * Then gets the content from the url structure and replaces the placeholders
	 * @param  string $template      Is the template and the folder
	 * @param  string $template_part is the additional string template part ie -results.php
	 * @return string output html
	 */
	public static function getTemplatePart($template, $template_part = null) {

		$file = ($template_part) ? $template . '-' . $template_part . '.php' : $template . '.php';

		$replace_text = array(
			'{{title}}'		=> parent::$content->selected_content['content']['page_title'],
			'{{content}}'	=> parent::$content->selected_content['content']['page_content'],
			'{{form}}'		=> parent::$content->selected_content['content']['form'],
			'{{results}}'	=> parent::$content->selected_content['content']['results'],	
		);
				
		$html ='';
		ob_start();
		include_once($_SERVER['DOCUMENT_ROOT'] . '/views/' . $template . '/' . $file);	
        $html .= ob_get_clean();
        
        /// You could then str_replace...
		$html = strtr($html, $replace_text);

		echo $html;

	}

	

	

}

